//
//  ModeSelectionViewController.h
//  VideoSmile
//
//  Created by Водолазкий В.В. on 09.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModeSelectionViewController : UIViewController
- (IBAction)musicSelected:(id)sender;

- (IBAction)animationSelected:(id)sender;
@end
