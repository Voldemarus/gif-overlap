//
//  TreeViewController.m
//  VideoSmile
//
//  Created by Водолазкий В.В. on 12.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "TreeViewController.h"
#import "TreeViewTableViewCell.h"
#import "DebugPrint.h"
#import "Preferences.h"

@interface TreeViewController () {
	NSFetchedResultsController	*controller;
	BOOL showImages;
	RATreeView *treeView;
	Preferences *prefs;
}

@end

@implementation TreeViewController

- (id) initForImageSelection:(BOOL)forImage
{
	if (self = [super init]) {
		showImages = forImage;
		prefs = [Preferences sharedPreferences];
		ANiGIFDataBase *src = [ANiGIFDataBase sharedDatabase];
		if (forImage) {
			controller = src.imageRootController;
		} else {
			controller = src.soundRootController;
		}
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	treeView = [[RATreeView alloc] initWithFrame:self.view.bounds];
	treeView.delegate = self;
	treeView.dataSource = self;
	treeView.separatorStyle = RATreeViewCellSeparatorStyleSingleLine;
	[treeView setBackgroundColor:[UIColor colorWithWhite:0.27 alpha:1.0]];
	[treeView registerNib:[UINib nibWithNibName:NSStringFromClass([RATableViewCell class]) bundle:nil] forCellReuseIdentifier:NSStringFromClass([RATableViewCell class])];

	
	
	//	[self.view insertSubview:treeView atIndex:0];
	[self.view addSubview:treeView];
	NSError *error = nil;
	[controller performFetch:&error];
	if (error) {
		DLog(@"Error during fetch - %@", [error localizedDescription]);
	}
	[treeView reloadData];
	
}

- (void)viewWillAppear:(BOOL)animated
{
	[super viewWillAppear:animated];
	
	int systemVersion = [[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."][0] intValue];
	if (systemVersion >= 7 && systemVersion < 8) {
		CGRect statusBarViewRect = [[UIApplication sharedApplication] statusBarFrame];
		float heightPadding = statusBarViewRect.size.height+self.navigationController.navigationBar.frame.size.height;
		treeView.contentInset = UIEdgeInsetsMake(heightPadding, 0.0, 0.0, 0.0);
		treeView.contentOffset = CGPointMake(0.0, -heightPadding);
	}
	
	treeView.frame = self.view.bounds;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - RATReeView delegate 

- (CGFloat)treeView:(RATreeView *)treeView heightForRowForItem:(id)item
{
	return 40;
}

- (BOOL)treeView:(RATreeView *)treeView canEditRowForItem:(id)item
{
	return NO;
}


- (UITableViewCell *)treeView:(RATreeView *)trv cellForItem:(id)item
{
	FileCategory *dataObject = item;
	
	NSInteger level = [treeView levelForCellForItem:item];
	FileCategory *selectedCategory = (showImages ? [prefs imageCategorySelected]
									  : [prefs soundCategorySelected]);
	BOOL selected = NO;
	if (selectedCategory && [dataObject isEqual:selectedCategory]) selected = YES;
	

	RATableViewCell *cell = [treeView dequeueReusableCellWithIdentifier:NSStringFromClass([RATableViewCell class])];
	
	[cell setupWithTitle:dataObject.caption detailText:@"11" level:level selected:selected];
	cell.selectionStyle = UITableViewCellSelectionStyleNone;

	
	
	return cell;
}

- (NSInteger)treeView:(RATreeView *)treeView numberOfChildrenOfItem:(id)item
{
	if (item == nil) {
		return [[controller fetchedObjects] count];
	}
	
	FileCategory *data = item;
	return [data.subcategories count];
}

- (id)treeView:(RATreeView *)treeView child:(NSInteger)index ofItem:(id)item
{
	FileCategory *data = item;
	if (item == nil) {
		return [[controller fetchedObjects] objectAtIndex:index];
	}
	return data.subcatOrdered[index];
}

- (void) treeView:(RATreeView *)treeView didSelectRowForItem:(id)item
{
	FileCategory *dobject = item;
	if (dobject.subcategories.count > 0) return;
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVfolderSelected object:item];
	[self.view removeFromSuperview];
}

@end
