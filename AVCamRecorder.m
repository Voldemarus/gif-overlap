/*
  */

#import "AVCamRecorder.h"
#import <AVFoundation/AVFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "DebugPrint.h"
#import "AppDelegate.h"

static void *AVCamRecorderConnectionsObserverContext = &AVCamRecorderConnectionsObserverContext;

static CGColorSpaceRef sDeviceRgbColorSpace = NULL;

static CGAffineTransform FCGetTransformForDeviceOrientation(UIDeviceOrientation orientation, BOOL mirrored)
{
	// Internal comment: This routine assumes that the native camera image is always coming from a UIDeviceOrientationLandscapeLeft (i.e. the home button is on the RIGHT, which equals AVCaptureVideoOrientationLandscapeRight!), although in the future this assumption may not hold; better to get video output's capture connection's videoOrientation property, and apply the transform according to the native video orientation
	
	// Also, it may be desirable to apply the flipping as a separate step after we get the rotation transform
	CGAffineTransform result;
	switch (orientation) {
		case UIDeviceOrientationPortrait:
		case UIDeviceOrientationFaceUp:
		case UIDeviceOrientationFaceDown:
			result = CGAffineTransformMakeRotation(M_PI_2);
			break;
		case UIDeviceOrientationPortraitUpsideDown:
			result = CGAffineTransformMakeRotation((3 * M_PI_2));
			break;
		case UIDeviceOrientationLandscapeLeft:
			result = mirrored ?  CGAffineTransformMakeRotation(M_PI) : CGAffineTransformIdentity;
			break;
		default:
			result = mirrored ? CGAffineTransformIdentity : CGAffineTransformMakeRotation(M_PI);
			break;
	}
	
	return result;
}


// an inline function to filter a CIImage through a filter chain; note that each image input attribute may have different source
static inline CIImage *RunFilter(CIImage *cameraImage, NSArray *filters)
{
	CIImage *currentImage = nil;
	NSMutableArray *activeInputs = [NSMutableArray array];
	
	for (CIFilter *filter in filters)
	{
		if ([filter isKindOfClass:[SourceVideoFilter class]])
		{
			[filter setValue:cameraImage forKey:kCIInputImageKey];
		}
//		else if ([filter isKindOfClass:[SourcePhotoFilter class]])
//		{
//			; // nothing to do here
//		}
		else
		{
			for (NSString *attrName in [filter imageInputAttributeKeys])
			{
				CIImage* top = [activeInputs lastObject];
				if (top)
				{
					[filter setValue:top forKey:attrName];
					[activeInputs removeLastObject];
				}
				else
					NSLog(@"failed to set %@ for %@", attrName, filter.name);
			}
		}
		
		currentImage = filter.outputImage;
		if (currentImage == nil)
			return nil;
		[activeInputs addObject:currentImage];
	}
	
	if (CGRectIsEmpty(currentImage.extent))
		return nil;
	return currentImage;
}



@interface AVCamRecorder () {
	dispatch_queue_t recordingQueue;
	AVAssetWriter		*assetWriter;
	
	AVCaptureConnection *videoConnection;
	AVCaptureConnection *audioConnection;
	
	AVAssetWriterInput	*videoWriteInput;
	AVAssetWriterInput	*audioWriteInput;
	AVAssetWriterInput	*audioMixInput;
	
	AVAssetWriterInputPixelBufferAdaptor *pixelBufferAdaptor;
	
	CMFormatDescriptionRef _currentAudioSampleBufferFormatDescription;
	
	AVCaptureAudioDataOutput	*audioOutput;
	CMTime				videoTimeStamp, audioTimeStamp;
	
	int64_t frameNumber;
	

	
}

@end

@implementation AVCamRecorder



- (id) initWithSession:(AVCaptureSession *)aSession outputFileURL:(NSURL *)anOutputFileURL
{
    self = [super init];
    if (self != nil) {
        AVCaptureVideoDataOutput *aMovieFileOutput = [[AVCaptureVideoDataOutput alloc] init];
		[aMovieFileOutput addObserver:self forKeyPath:@"connections" options:0 context:AVCamRecorderConnectionsObserverContext];
        if ([aSession canAddOutput:aMovieFileOutput])
            [aSession addOutput:aMovieFileOutput];
		
		aMovieFileOutput.alwaysDiscardsLateVideoFrames = YES;
		
		// Configure delegate to process output frames
		 recordingQueue = dispatch_queue_create("myQueue", NULL);
		[aMovieFileOutput setSampleBufferDelegate:self queue:recordingQueue];
		
		NSDictionary *outputSettings = [[NSDictionary alloc] initWithObjectsAndKeys:@(kCVPixelFormatType_32BGRA),kCVPixelBufferPixelFormatTypeKey,nil];
		[aMovieFileOutput setVideoSettings:outputSettings];

		_frameRateCalculator = [[FrameRateCalculator alloc] init];

		
		audioOutput = [[AVCaptureAudioDataOutput alloc] init];
		[audioOutput setSampleBufferDelegate:self queue:recordingQueue];
		if ([aSession canAddOutput:audioOutput]) {
			[aSession addOutput:audioOutput];
		} else {
			DLog(@"Cannot add audioOutput!");
		}
		
		[self setBlackboard:aMovieFileOutput];
		
		[self setSession:aSession];
		[self setOutputFileURL:anOutputFileURL];
		
		// create the CIContext instance, note that this must be done after _videoPreviewView is properly set up
		static dispatch_once_t onceToken;
		dispatch_once(&onceToken, ^{
			sDeviceRgbColorSpace = CGColorSpaceCreateDeviceRGB();
		});
		
		_eaglContext = [[EAGLContext alloc] initWithAPI:kEAGLRenderingAPIOpenGLES2];
		UIView *window = ((AppDelegate *)[UIApplication sharedApplication].delegate).window;

		_videoPreviewView = [[GLKView alloc] initWithFrame:window.bounds context:_eaglContext];
		_ciContext = [CIContext contextWithEAGLContext:_eaglContext options:@{kCIContextWorkingColorSpace : [NSNull null]} ];

		
   }
	DLog(@"1111 - outputs added");
	return self;
}

- (void) dealloc
{
    [[self session] removeOutput:[self blackboard]];
	[_blackboard removeObserver:self forKeyPath:@"connections"];
	
	if (_currentAudioSampleBufferFormatDescription)
		CFRelease(_currentAudioSampleBufferFormatDescription);
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
	if (context == AVCamRecorderConnectionsObserverContext) {
		// Whenever the Movie File Output's connections property changes, a connection has been added
		// or removed, such as when we switch cameras (remove an input, add a new input, which implicitly
		// severs a connection and forms a new one).  Here is where we do all of our one-time connection
		// set up.
		videoConnection = [object connectionWithMediaType:AVMediaTypeVideo];
		if (videoConnection) {
			// Opt in for video stabilization
			if ([videoConnection isVideoStabilizationSupported])
				[videoConnection setEnablesVideoStabilizationWhenAvailable:YES];
		}
	}
}

-(BOOL)recordsVideo
{
	//	videoConnection = [_blackboard connectionWithMediaType:AVMediaTypeVideo];
	//	return [videoConnection isActive];
	return YES;
}

-(BOOL)recordsAudio
{
	//	audioConnection = [_blackboard connectionWithMediaType:AVMediaTypeAudio];
	//	return [audioConnection isActive];
	return YES;
}

-(BOOL)isRecording
{
    return ([self.session isRunning] && (assetWriter.status == AVAssetWriterStatusWriting));
}

-(void)startRecordingWithOrientation:(AVCaptureVideoOrientation)videoOrientation;
{
//    videoConnection = [_blackboard connectionWithMediaType:AVMediaTypeVideo];
//    if ([videoConnection isVideoOrientationSupported])
//        [videoConnection setVideoOrientation:videoOrientation];
	
	if (!assetWriter) {
		[self setupVideoRecordingWithSampleBuffer:nil];
	}
	BOOL started = [assetWriter startWriting];
	if (started) {
		DLog(@"timetamp = %lld",timestamp.value);
		[assetWriter startSessionAtSourceTime:timestamp];
		if ([[self delegate] respondsToSelector:@selector(recorderRecordingDidBegin:)]) {
			[[self delegate] recorderRecordingDidBegin:self];
		}
	} else {
		DLog(@"Cannot start Writer! - status = %d",assetWriter.status);
	}
}

-(void)stopRecording
{
	[self.session stopRunning];
	[assetWriter finishWritingWithCompletionHandler:^{
		NSError *error = nil;
		if ([[self delegate] respondsToSelector:@selector(recorder:recordingDidFinishToOutputFileURL:error:)]) {
			[[self delegate] recorder:self recordingDidFinishToOutputFileURL:self.outputFileURL error:error];
			assetWriter = nil;		// dismissed! Should be created new for each file
			[self.session startRunning]; // restart to allow Preview work
		}
	}];
}


 -(void)setupVideoRecordingWithSampleBuffer:(CMSampleBufferRef)sampleBuffer
{
	if (!assetWriter) {
		DLog(@"Init assetWriter");
		NSError *error = nil;
		NSString *path = [self.outputFileURL path];
		if ([[NSFileManager defaultManager] fileExistsAtPath:path]) {
			[[NSFileManager defaultManager] removeItemAtPath:path error:&error];
			if (error) {
				DLog(@"Cannot delete file %@ - %@", path, [error localizedDescription]);
			}
		}
		assetWriter = [[AVAssetWriter alloc] initWithURL:self.outputFileURL fileType:AVFileTypeQuickTimeMovie error:&error];
		if (error) {
			DLog(@"error = %@",[error localizedDescription]);
			return;
		}
		assetWriter.shouldOptimizeForNetworkUse = YES;
			 
//		CGFloat videoBitRate = 437500 * 8;
//		NSInteger videoFrameRate = 30;
//		NSDictionary *compressionSettings = @{ AVVideoAverageBitRateKey : @(videoBitRate),
//											   AVVideoMaxKeyFrameIntervalKey : @(videoFrameRate) };
//		CMFormatDescriptionRef formatDescription = CMSampleBufferGetFormatDescription(sampleBuffer);
//		CMVideoDimensions videoDimensions = CMVideoFormatDescriptionGetDimensions(formatDescription);
		
//		NSDictionary *videoSettings = @{ AVVideoCodecKey : AVVideoCodecH264,
//									 AVVideoScalingModeKey : AVVideoScalingModeResizeAspectFill,
//									 AVVideoWidthKey : @(videoDimensions.width),
//									 AVVideoHeightKey : @(videoDimensions.height),
//									 AVVideoCompressionPropertiesKey : compressionSettings };

		NSDictionary *videoSettings = @{
										AVVideoWidthKey :	@(640),
										AVVideoHeightKey:	@(480),
										AVVideoCodecKey:	AVVideoCodecH264,
										};
		
		
//		NSDictionary *audioSettings = [audioOutput
//									   recommendedAudioSettingsForAssetWriterWithOutputFileType:AVFileTypeMPEG4];
//			 
    /*
	 AudioChannelLayout acl;
	 bzero( &acl, sizeof(acl));
	 acl.mChannelLayoutTag = kAudioChannelLayoutTag_Mono;
	 
	 
	 NSDictionary* audioSettings = [NSDictionary dictionaryWithObjectsAndKeys:
	 [ NSNumber numberWithInt: kAudioFormatMPEG4AAC], AVFormatIDKey,
	 [ NSNumber numberWithInt: 1 ], AVNumberOfChannelsKey,
	 [ NSNumber numberWithFloat: 44100.0 ], AVSampleRateKey,
	 [ NSData dataWithBytes: &acl length: sizeof( acl ) ], AVChannelLayoutKey,
	 [ NSNumber numberWithInt: 64000 ], AVEncoderBitRateKey,
	 nil];
	 */
		
		
		videoWriteInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeVideo outputSettings:videoSettings];
		videoWriteInput.expectsMediaDataInRealTime = YES;
		
		
		// create a pixel buffer adaptor for the asset writer; we need to obtain pixel buffers for rendering later from its pixel buffer pool
//		_assetWriterInputPixelBufferAdaptor = [AVAssetWriterInputPixelBufferAdaptor assetWriterInputPixelBufferAdaptorWithAssetWriterInput:videoWriteInput sourcePixelBufferAttributes:
//											   [NSDictionary dictionaryWithObjectsAndKeys:
//												[NSNumber numberWithInteger:kCVPixelFormatType_32BGRA], (id)kCVPixelBufferPixelFormatTypeKey,
//												[NSNumber numberWithUnsignedInteger:640], (id)kCVPixelBufferWidthKey,
//												[NSNumber numberWithUnsignedInteger:480], (id)kCVPixelBufferHeightKey,
//												(id)kCFBooleanTrue, (id)kCVPixelFormatOpenGLESCompatibility,
//												nil]];
		
		
		/* I'm going to push pixel buffers to it, so will need a
   AVAssetWriterPixelBufferAdaptor, to expect the same 32BGRA input as I've
   asked the AVCaptureVideDataOutput to supply */
		_assetWriterInputPixelBufferAdaptor =
		[[AVAssetWriterInputPixelBufferAdaptor alloc]
		 initWithAssetWriterInput:videoWriteInput
		 sourcePixelBufferAttributes:
		 [NSDictionary dictionaryWithObjectsAndKeys:
		  [NSNumber numberWithInt:kCVPixelFormatType_32BGRA],
		  kCVPixelBufferPixelFormatTypeKey,
		  nil]];
		
		CGAffineTransform affine = CGAffineTransformMakeRotation (M_PI * 90 / 180.0f);
		videoWriteInput.transform = affine;//CGAffineTransformIdentity;
		
		if ([assetWriter canApplyOutputSettings:videoSettings forMediaType:AVMediaTypeVideo]) {
		
			if ([assetWriter canAddInput:videoWriteInput]) {
				[assetWriter addInput:videoWriteInput];
			}
		}
		
		size_t layoutSize = 0;
		const AudioChannelLayout *channelLayout = CMAudioFormatDescriptionGetChannelLayout(_currentAudioSampleBufferFormatDescription, &layoutSize);
		const AudioStreamBasicDescription *basicDescription = CMAudioFormatDescriptionGetStreamBasicDescription(_currentAudioSampleBufferFormatDescription);
		
		NSData *channelLayoutData = [NSData dataWithBytes:channelLayout length:layoutSize];
		
		// record the audio at AAC format, bitrate 64000, sample rate and channel number using the basic description from the audio samples
		NSDictionary *audioSettings = [NSDictionary dictionaryWithObjectsAndKeys:
									   [NSNumber numberWithInteger:kAudioFormatMPEG4AAC], AVFormatIDKey,
									   [NSNumber numberWithInteger:basicDescription->mChannelsPerFrame], AVNumberOfChannelsKey,
									   [NSNumber numberWithFloat:basicDescription->mSampleRate], AVSampleRateKey,
									   [NSNumber numberWithInteger:64000], AVEncoderBitRateKey,
									   channelLayoutData, AVChannelLayoutKey,
									   nil];
		
		if ([assetWriter canApplyOutputSettings:audioSettings forMediaType:AVMediaTypeAudio]) {
			audioWriteInput = [[AVAssetWriterInput alloc] initWithMediaType:AVMediaTypeAudio outputSettings:audioSettings];
			audioWriteInput.expectsMediaDataInRealTime = YES;
			DLog(@"audio connected! - %@",audioSettings);
		}

		
		if ([assetWriter canApplyOutputSettings:audioSettings forMediaType:AVMediaTypeAudio]) {
			if ([assetWriter canAddInput:audioWriteInput]) {
				[assetWriter addInput:audioWriteInput];
			}
		}
		frameNumber = 0;
	 }
 }



- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputSampleBuffer:(CMSampleBufferRef)sampleBuffer
	   fromConnection:(AVCaptureConnection *)connection
{
	
	CMFormatDescriptionRef formatDesc = CMSampleBufferGetFormatDescription(sampleBuffer);
	CMMediaType mediaType = CMFormatDescriptionGetMediaType(formatDesc);
	
	// write the audio data if it's from the audio connection
	if (mediaType == kCMMediaType_Audio) {
		CMFormatDescriptionRef tmpDesc = _currentAudioSampleBufferFormatDescription;
		_currentAudioSampleBufferFormatDescription = formatDesc;
		CFRetain(_currentAudioSampleBufferFormatDescription);
		
		if (tmpDesc)
			CFRelease(tmpDesc);
		
		// we need to retain the sample buffer to keep it alive across the different queues (threads)
		if (assetWriter &&
			audioWriteInput.readyForMoreMediaData ) {
			BOOL recorded = [audioWriteInput appendSampleBuffer:sampleBuffer];
			if (!recorded) {
				DLog(@"Cannot store audio data!");
				[self stopRecording];
			}
		}
		
		return;
	}
	
	// if not from the audio capture connection, handle video writing
	timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
	[_frameRateCalculator calculateFramerateAtTimestamp:timestamp];
	
	// update the video dimensions information
	_currentVideoDimensions = CMVideoFormatDescriptionGetDimensions(formatDesc);
	
	
	CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
	CIImage *sourceImage = [CIImage imageWithCVPixelBuffer:(CVPixelBufferRef)imageBuffer options:nil];
	
	// run the filter through the filter chain
	//	CIImage *filteredImage = RunFilter(sourceImage, _activeFilters);
	
	CGRect sourceExtent = sourceImage.extent;
	
	CGFloat sourceAspect = sourceExtent.size.width / sourceExtent.size.height;
	CGFloat previewAspect = 640  / 480;
	
	// we want to maintain the aspect radio of the screen size, so we clip the video image
	CGRect drawRect = sourceExtent;
	if (sourceAspect > previewAspect)
	{
		// use full height of the video image, and center crop the width
		drawRect.origin.x += (drawRect.size.width - drawRect.size.height * previewAspect) / 2.0;
		drawRect.size.width = drawRect.size.height * previewAspect;
	}
	else
	{
		// use full width of the video image, and center crop the height
		drawRect.origin.y += (drawRect.size.height - drawRect.size.width / previewAspect) / 2.0;
		drawRect.size.height = drawRect.size.width / previewAspect;
	}
	
	if (assetWriter)	{
		
		CVPixelBufferRef renderedOutputPixelBuffer = NULL;
		
		OSStatus err = CVPixelBufferPoolCreatePixelBuffer(nil, _assetWriterInputPixelBufferAdaptor.pixelBufferPool, &renderedOutputPixelBuffer);
		if (err)
		{
			NSLog(@"Cannot obtain a pixel buffer from the buffer pool");
			return;
		}
		
		// render the filtered image back to the pixel buffer (no locking needed as CIContext's render method will do that
		if (filteredImage)
			[_ciContext render:filteredImage toCVPixelBuffer:renderedOutputPixelBuffer bounds:[filteredImage extent] colorSpace:sDeviceRgbColorSpace];
		
		// pass option nil to enable color matching at the output, otherwise the color will be off
		CIImage *drawImage = [CIImage imageWithCVPixelBuffer:renderedOutputPixelBuffer options:nil];
		
		[_videoPreviewView bindDrawable];
		[_ciContext drawImage:drawImage inRect:_videoPreviewViewBounds fromRect:drawRect];
		[_videoPreviewView display];
		
		
		// self.currentVideoTime = timestamp;
		
		// write the video data
		if (videoWriteInput.readyForMoreMediaData)
			[_assetWriterInputPixelBufferAdaptor appendPixelBuffer:renderedOutputPixelBuffer withPresentationTime:timestamp];
		
		CVPixelBufferRelease(renderedOutputPixelBuffer);
	}
	
//	CFRetain(sampleBuffer);
//	// init output file parameters once
//	//	[self setupVideoRecordingWithSampleBuffer:sampleBuffer];
//
//	timestamp = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
//
//
//	CVImageBufferRef imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
//
//	// a very dense way to keep track of the time at which this frame
//	// occurs relative to the output stream, but it's just an example!
//
//	if(connection == videoConnection && videoWriteInput.readyForMoreMediaData && imageBuffer) {
//		[pixelBufferAdaptor appendPixelBuffer:imageBuffer
//						 withPresentationTime:CMTimeMake(frameNumber, 25)];
//		frameNumber++;
//	}
//	
//	CFRelease(sampleBuffer);
}

- (void) writeSampleBuffer:(CMSampleBufferRef)sampleBuffer ofType:(NSString *)mediaType
{
	CMTime presentationTime = CMSampleBufferGetPresentationTimeStamp(sampleBuffer);
	
//	if ( assetWriter.status == AVAssetWriterStatusUnknown ) {
//		
//		if ([assetWriter startWriting]) {
//			[assetWriter startSessionAtSourceTime:presentationTime];
//		} else {
//			NSLog(@"Error writing initial buffer");
//		}
//	}
	if ([self isRecording]) {
		if ( assetWriter.status == AVAssetWriterStatusWriting ) {
			
			if (mediaType == AVMediaTypeVideo) {
				if (videoWriteInput.readyForMoreMediaData) {
					if (![videoWriteInput appendSampleBuffer:sampleBuffer]) {
						DLog(@"Error writing video buffer");
					}
					videoTimeStamp = presentationTime;
				}
			}
			else if (mediaType == AVMediaTypeAudio) {
				if (audioWriteInput.readyForMoreMediaData) {
					if (![audioWriteInput appendSampleBuffer:sampleBuffer]) {
						DLog(@"Error writing audio buffer");
					}
					audioTimeStamp = presentationTime;
				}
			}
		}
	}

}



@end

