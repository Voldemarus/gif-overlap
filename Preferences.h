//
//  Preferences.h
//  Portal Picture
//
//  Created by Водолазкий В.В. on 30.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ANiGIFDataBase.h"

typedef enum {
	VSModeReadyToRecord = 0,
	VSModeRecordInProgress,
	VSModeReadyToPlay,
	VSModePlayInProgress
} VSMode;


//
// These notificators sends variable' states as NSNumber* in [note object]
// to simplify extracting current value of parameter
//
extern NSString * const VVVRecorrdTimeoutFired;			// Record has been stopped
extern NSString * const VVVimageSelected;				// Image has been selected to overlap
extern NSString * const VVVtuneSelected;				// Sound selected to mix into composition
extern NSString * const VVVfolderSelected;				// Database category selected
extern NSString * const VVVselectImageDatabase;			// show to user animation database
extern NSString * const VVVselectMusicDatabase;			// show to user music database
extern NSString * const VVVshowCategoryList;			// Request to show list of categories
extern NSString * const VVVcloseCategorySelector;
extern NSString * const VVVstampModeChanged;			// Inform ViewController that stampMode Changed
extern NSString * const VVVmainButoonPressed;			// MainMode button pressed
extern NSString * const VVVmainModeChanged;				// ViewController changed mode of operation;

@interface Preferences : NSObject

+ (Preferences *) sharedPreferences;
- (void) flush;


// Record timeout
@property (nonatomic, readwrite) NSTimeInterval recordTimeout;
@property (nonatomic, readwrite) BOOL stopButtonAtLeft;

// GIF database settings

@property (nonatomic, readwrite, retain) FileCategory *imageCategorySelected;
@property (nonatomic, readwrite, retain) FileCategory *soundCategorySelected;
@property (nonatomic, readonly, retain) NSMutableSet *imagesSelected;
@property (nonatomic, readonly, retain) NSMutableSet *soundsSelected;

- (void) addSelectedImage:(FileReference *) fref;
- (void) removeSelectedImage:(FileReference *) fref;
- (BOOL) imageIsSelected:(FileReference *)fref;

- (void) addSelectedSound:(FileReference *)fref;
- (void) removeSelectedSound:(FileReference *)fref;
- (BOOL) soundIsSelected:(FileReference *)fref;





@end
