//
//  ModeSelectionViewController.m
//  VideoSmile
//
//  Created by Водолазкий В.В. on 09.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "ModeSelectionViewController.h"
#import "Preferences.h"

@interface ModeSelectionViewController ()

@end

@implementation ModeSelectionViewController

- (id) init
{
	if (self = [super initWithNibName:@"ModeSelectionViewController" bundle:nil]) {
		
	}
	return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)musicSelected:(id)sender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVselectMusicDatabase object:nil];
}

- (IBAction)animationSelected:(id)sender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVselectImageDatabase object:nil];
}
@end
