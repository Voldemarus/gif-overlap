//
//  AppDelegate.m
//  VideoSmile
//
//  Created by Водолазкий В.В. on 05.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "AppDelegate.h"

#import <HockeySDK/HockeySDK.h>

@interface AppDelegate () {
	Preferences *prefs;
}

@end

@implementation AppDelegate

@synthesize motionManager = _motionManager;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	// Override point for customization after application launch.
	[[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"a8dd1eb6c95bd4539ecf19e4a65c1e7e"];
	
	[[BITHockeyManager sharedHockeyManager] testIdentifier];
	
	[[BITHockeyManager sharedHockeyManager] startManager];
	[[BITHockeyManager sharedHockeyManager].authenticator
											authenticateInstallation];

	
	
	self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	self.window.backgroundColor = [UIColor blackColor];
	
	self.cameraView = [[FHViewController alloc] init];
	[self.window setRootViewController:self.cameraView];
	[self.window makeKeyAndVisible];
	
	prefs = [Preferences sharedPreferences];
	
	// initialize the motion manager
	_motionManager = [[CMMotionManager alloc] init];
	_motionManager.deviceMotionUpdateInterval = 0.1; // 10 Hz
	
	self.workingMode = VSModeReadyToRecord;
	
	return YES;
}

- (UIDeviceOrientation)realDeviceOrientation
{
	CMDeviceMotion *deviceMotion = _motionManager.deviceMotion;
	
	double x = deviceMotion.gravity.x;
	double y = deviceMotion.gravity.y;
	
	if (fabs(y) >= fabs(x))
	{
		if (y >= 0)
			return UIDeviceOrientationPortraitUpsideDown;
		else
			return UIDeviceOrientationPortrait;
	}
	else
	{
		if (x >= 0)
			return UIDeviceOrientationLandscapeRight;
		else
			return UIDeviceOrientationLandscapeLeft;
	}
}


- (void)applicationWillResignActive:(UIApplication *)application {
	// Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
	// Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
	 [_motionManager stopDeviceMotionUpdates];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
	// Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
	// If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
	[prefs flush];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
	// Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	// Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
	[_motionManager startDeviceMotionUpdates];
}

- (void)applicationWillTerminate:(UIApplication *)application {
	// Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
	[prefs flush];
}



@end
