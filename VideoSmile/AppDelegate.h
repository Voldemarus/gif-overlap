//
//  AppDelegate.h
//  VideoSmile
//
//  Created by Водолазкий В.В. on 05.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMotion/CoreMotion.h>

#import "FHViewController.h"
#import "Preferences.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate> {
	CMMotionManager *_motionManager;
}

@property (strong, nonatomic) UIWindow *window;
@property (strong, readonly, nonatomic) CMMotionManager *motionManager;

@property (nonatomic, readwrite) VSMode workingMode;

@property (nonatomic, retain) FHViewController *cameraView;
@property (assign, readonly, nonatomic) UIDeviceOrientation realDeviceOrientation;
@end

