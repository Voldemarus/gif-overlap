/*
     File: FHViewController.h
 
 
 */

#import <AVFoundation/AVFoundation.h>
#import <CoreText/CoreText.h>
#import <GLKit/GLKit.h>
#import <UIKit/UIKit.h>
#import "FilterStack.h"
#import "FrameRateCalculator.h"
#import "SettingsController.h"
#import "Preferences.h"
#import "ANiGIFDataBase.h"
#import "MediaSelectorViewController.h"
#import "TreeViewController.h"

@interface FHViewController : UIViewController
				<AVCaptureVideoDataOutputSampleBufferDelegate,
					AVCaptureAudioDataOutputSampleBufferDelegate,
					AVAudioPlayerDelegate,
					SettingsControllerDelegate>
{
@private
    GLKView *_videoPreviewView;    
    CIContext *_ciContext;
    EAGLContext *_eaglContext;
    CGRect _videoPreviewViewBounds;
    
    AVCaptureDevice *_audioDevice;
    AVCaptureDevice *_videoDevice;
    AVCaptureSession *_captureSession;

    AVAssetWriter *_assetWriter;
	AVAssetWriterInput *_assetWriterAudioInput;
    AVAssetWriterInput *_assetWriterVideoInput;
    AVAssetWriterInputPixelBufferAdaptor *_assetWriterInputPixelBufferAdaptor;

    dispatch_queue_t _captureSessionQueue;    
    UIBackgroundTaskIdentifier _backgroundRecordingID;
    
    FilterStack *_filterStack;
    NSArray *_activeFilters;
    
    BOOL _videoWritingStarted;
    CMTime _videoWrtingStartTime;
    CMFormatDescriptionRef _currentAudioSampleBufferFormatDescription;
    CMVideoDimensions _currentVideoDimensions;
    CMTime _currentVideoTime;

	BOOL stampModeIsActive;
	
	FrameRateCalculator *_frameRateCalculator;
     
    BOOL _filterPopoverVisibleBeforeRotation;
    BOOL _settingsPopoverVisibleBeforeRotation;
	
	NSTimer *recordTimeoutTimer;
	NSTimeInterval timeout;
	Preferences *prefs;
	
	FileReference *imageFileRef;
	FileReference *soundFileRef;
	
	NSURL *recordedFileURL;

}

@property (nonatomic,strong) AVCaptureSession *session;
@property (nonatomic,assign) AVCaptureVideoOrientation orientation;
@property (nonatomic,strong) AVCaptureDeviceInput *videoInput;
@property (nonatomic,strong) AVCaptureDeviceInput *audioInput;

@property (nonatomic, retain) TreeViewController *categorySelector;

@property (strong, nonatomic) IBOutlet UIToolbar *toolbar;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *recordStopButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *fpsLabel;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *settingsButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *filtersButton;

@property (strong, nonatomic) UIPopoverController *filterListPopoverController;
@property (strong, nonatomic) UINavigationController *filterListNavigationController;

@property (weak, nonatomic) IBOutlet UIView *previewView;
@property (weak, nonatomic) IBOutlet UIProgressView *timeRemained;

@property (nonatomic, strong) MediaSelectorViewController *modeSelector;

@property (strong, nonatomic) IBOutlet UIView *toolsView;
@property (weak, nonatomic) IBOutlet UIButton *toolsRecordButton;
@property (weak, nonatomic) IBOutlet UILabel *toolsRecordPrompt;
- (IBAction)toggleRecording:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *gifPreview;

@property (weak, nonatomic) IBOutlet UIButton *playSoundButton;
@property (weak, nonatomic) IBOutlet UILabel *selectedTuneName;
@property (nonatomic, retain) AVAudioPlayer *player;			// audio preview
@property (weak, nonatomic) IBOutlet UIImageView *stampModeIndicator;

@property (weak, nonatomic) IBOutlet UIButton *cameraDirectionButton;
- (IBAction)cameraDirectionButtonPressed:(id)sender;

- (IBAction)playSoundButtonPressed:(id)sender;
- (IBAction)settingsButtonPressed:(id)sender;
@property (assign, atomic) CMTime currentVideoTime;

- (IBAction)recordStopAction:(UIBarButtonItem *)sender event:(UIEvent *)event;
- (IBAction)settingsAction:(UIBarButtonItem *)sender event:(UIEvent *)event;
- (IBAction)filtersAction:(UIBarButtonItem *)sender event:(UIEvent *)event;

@property (strong, nonatomic) UIPopoverController *settingsPopoverController;
@property (strong, nonatomic) UINavigationController *settingsNavigationController;
@end

// for telling UI controllers that it has started the capture session, not necessarily meaning the capture session has succeeded started
extern NSString *const FHViewControllerDidStartCaptureSessionNotification;
