//
//  ANiGIFDataBase.h
//  VideoSmile
//
//  Created by Водолазкий В.В. on 06.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

#import "FileCategory.h"
#import "FileReference.h"


@interface ANiGIFDataBase : NSObject

@property (nonatomic, retain, readonly) NSArray *categories;
@property (nonatomic, retain, readonly) NSArray *sounds;		// returns names of sounds

+ (ANiGIFDataBase *) sharedDatabase;							// unified access point

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;


- (NSFetchedResultsController *) imageRootController;
- (NSFetchedResultsController *) soundRootController;
- (NSFetchedResultsController *) imagesForCategory:(FileCategory *)cat;
- (NSFetchedResultsController *) soundsForCategory:(FileCategory *)cat;

- (NSFetchedResultsController *) filesForCategory:(FileCategory *)cat;
- (UIImage *) animatedGIFForFileRef:(FileReference *)fileRef;

- (NSString *)soundFileNameFortuneName:(NSString *)aName;

@end
