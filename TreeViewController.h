//
//  TreeViewController.h
//  VideoSmile
//
//  Created by Водолазкий В.В. on 12.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RATreeView.h"
#import "ANiGIFDataBase.h"
#import "RATableViewCell.h"

@interface TreeViewController : UIViewController <RATreeViewDataSource, RATreeViewDelegate>

- (id) initForImageSelection:(BOOL)forImage;


@end
