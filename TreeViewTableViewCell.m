//
//  TreeViewCellTableViewCell.m
//  VideoSmile
//
//  Created by Водолазкий В.В. on 12.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "TreeViewTableViewCell.h"

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@implementation TreeViewTableViewCell

- (id) initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
	if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
		self.categoryLabel = [[UILabel alloc] initWithFrame:CGRectMake(39, 4, 79, 31)];
		[self.backgroundView addSubview:self.categoryLabel];
		self.disclosureIcon = [[UIImageView alloc] initWithFrame:CGRectMake(4, 19, 25, 20)];
		self.disclosureIcon.contentMode = UIViewContentModeScaleToFill;
		self.disclosureIcon.image = [UIImage imageNamed:@"DisclosureIcon.png"];
		[self.backgroundView addSubview:self.disclosureIcon];
	}
	return self;
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.selectedBackgroundView = [UIView new];
	self.selectedBackgroundView.backgroundColor = [UIColor clearColor];
}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (void) setupViewWithCategoryName:(NSString *)caption isSelected:(BOOL) selected andImage:(BOOL)showimage
{
	CGRect labelFrame = self.categoryLabel.frame;
	CGRect imageFrame = self.disclosureIcon.frame;
	if (showimage) {
		self.disclosureIcon.alpha = 1.0;
		labelFrame.origin.x = imageFrame.origin.x + imageFrame.size.width + 3;
	} else {
		self.disclosureIcon.alpha = 0.0;
		labelFrame.origin.x = imageFrame.origin.x;
	}
	self.categoryLabel.frame = labelFrame;
	self.categoryLabel.text = caption;
	NSLog(@"caption = %@", caption);
	self.backgroundColor = (selected ? UIColorFromRGB(0xD1EEFC) : UIColorFromRGB(0xF7F7F7));

}

@end
