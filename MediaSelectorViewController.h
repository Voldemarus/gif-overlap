//
//  MediaSelectorViewController.h
//  VideoSmile
//
//  Created by Водолазкий В.В. on 12.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ANiGIFDataBase.h"


@interface MediaSelectorViewController : UIViewController  <UICollectionViewDataSource,
											UICollectionViewDelegate> {
	BOOL imageMode;
								
}

@property (nonatomic, readonly) BOOL imageMode;

@property (weak, nonatomic) IBOutlet UICollectionView *collection;
@property (nonatomic, strong) NSFetchedResultsController *fetchController;

@property (weak, nonatomic) IBOutlet UILabel *categoryLabel;
@property (weak, nonatomic) IBOutlet UIButton *categoryButton;
- (IBAction)categorySelectButtonPressed:(id)sender;
- (IBAction)closeModePressed:(id)sender;

- (IBAction)imageModeSelected:(id)sender;
- (IBAction)tuneModeSelected:(id)sender;

- (IBAction)stampModePressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *mainModeButton;
- (IBAction)mainModeButtonPRessed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *backButton;
- (IBAction)backButtonPressed:(id)sender;



- (id) initWithImageList;
- (id) initWithSoundList;

- (void) showViewAtBottom;

@end
