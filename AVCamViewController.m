/*
     File: AVCamViewController.m
 Abstract: A view controller that coordinates the transfer of information between the user interface and the capture manager.
  Version: 2.1
 
 Disclaimer: IMPORTANT:  This Apple software is supplied to you by Apple
 Inc. ("Apple") in consideration of your agreement to the following
 terms, and your use, installation, modification or redistribution of
 this Apple software constitutes acceptance of these terms.  If you do
 not agree with these terms, please do not use, install, modify or
 redistribute this Apple software.
 
 In consideration of your agreement to abide by the following terms, and
 subject to these terms, Apple grants you a personal, non-exclusive
 license, under Apple's copyrights in this original Apple software (the
 "Apple Software"), to use, reproduce, modify and redistribute the Apple
 Software, with or without modifications, in source and/or binary forms;
 provided that if you redistribute the Apple Software in its entirety and
 without modifications, you must retain this notice and the following
 text and disclaimers in all such redistributions of the Apple Software.
 Neither the name, trademarks, service marks or logos of Apple Inc. may
 be used to endorse or promote products derived from the Apple Software
 without specific prior written permission from Apple.  Except as
 expressly stated in this notice, no other rights or licenses, express or
 implied, are granted by Apple herein, including but not limited to any
 patent rights that may be infringed by your derivative works or by other
 works in which the Apple Software may be incorporated.
 
 The Apple Software is provided by Apple on an "AS IS" basis.  APPLE
 MAKES NO WARRANTIES, EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION
 THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE, REGARDING THE APPLE SOFTWARE OR ITS USE AND
 OPERATION ALONE OR IN COMBINATION WITH YOUR PRODUCTS.
 
 IN NO EVENT SHALL APPLE BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL
 OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION,
 MODIFICATION AND/OR DISTRIBUTION OF THE APPLE SOFTWARE, HOWEVER CAUSED
 AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE),
 STRICT LIABILITY OR OTHERWISE, EVEN IF APPLE HAS BEEN ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (C) 2013 Apple Inc. All Rights Reserved.
 
 */

#import "AVCamViewController.h"
#import "AVCamCaptureManager.h"
#import "AVCamRecorder.h"
#import <AVFoundation/AVFoundation.h>
#import "UIImage+animatedGIF.h"
#import <ImageIO/ImageIO.h>
#import "DebugPrint.h"

static void *AVCamFocusModeObserverContext = &AVCamFocusModeObserverContext;

@interface AVCamViewController () <UIGestureRecognizerDelegate> {
	NSTimer *recordTimeoutTimer;
	NSTimeInterval timeout;
	Preferences *prefs;
	
	FileReference *imageFileRef;
	FileReference *soundFileRef;
}
@end

@interface AVCamViewController (InternalMethods)
- (void)tapToAutoFocus:(UIGestureRecognizer *)gestureRecognizer;
- (void)tapToContinouslyAutoFocus:(UIGestureRecognizer *)gestureRecognizer;
- (void)updateButtonStates;
@end

@interface AVCamViewController (AVCamCaptureManagerDelegate) <AVCamCaptureManagerDelegate>
@end

@implementation AVCamViewController


- (id) init
{
	if (self = [super initWithNibName:@"AVCamViewController" bundle:nil]) {
		prefs = [Preferences sharedPreferences];
		NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
		[nc addObserver:self selector:@selector(updateGifPreview:) name:VVVimageSelected object:nil];
		[nc addObserver:self selector:@selector(selectImageDatabase:) name:VVVselectImageDatabase object:nil];
		[nc addObserver:self selector:@selector(selectMusicDatabase:) name:VVVselectMusicDatabase object:nil];
		[nc addObserver:self selector:@selector(updateTunePreview:) name:VVVtuneSelected object:nil];
		[nc addObserver:self selector:@selector(showCategorySelector:) name:VVVshowCategoryList object:nil];
		[nc addObserver:self selector:@selector(closeCategorySelector:) name:VVVcloseCategorySelector object:nil];
	}
	return self;
}


- (NSString *)stringForFocusMode:(AVCaptureFocusMode)focusMode
{
	NSString *focusString = @"";
	
	switch (focusMode) {
		case AVCaptureFocusModeLocked:
			focusString = @"locked";
			break;
		case AVCaptureFocusModeAutoFocus:
			focusString = @"auto";
			break;
		case AVCaptureFocusModeContinuousAutoFocus:
			focusString = @"continuous";
			break;
	}
	
	return focusString;
}

- (void)dealloc
{
    [self removeObserver:self forKeyPath:@"captureManager.videoInput.device.focusMode"];
	
}

- (void)viewDidLoad
{
	
	self.modeSelector = [[ModeSelectionViewController alloc] init];
	
	self.imageSelector = [[MediaSelectorViewController alloc] initWithImageList];
	self.soundSelector = [[MediaSelectorViewController alloc] initWithSoundList];
	CGRect screenBounds = [[UIScreen mainScreen] bounds];
	CGRect frame = self.imageSelector.view.frame;
	frame.origin.x = 0;
	frame.origin.y = screenBounds.size.height;
	self.imageSelector.view.frame = frame;
	
	frame = self.soundSelector.view.frame;
	frame.origin.x = 0;
	frame.origin.y = screenBounds.size.height;
	self.soundSelector.view.frame = frame;
	
	frame = self.modeSelector.view.frame;
	frame.origin.x = 0;
	frame.origin.y = screenBounds.size.height - frame.size.height;
	self.modeSelector.view.frame = frame;
	
	[self.view addSubview:self.modeSelector.view];
	[self.view addSubview:self.imageSelector.view];
	[self.view addSubview:self.soundSelector.view];
	
	if ([self captureManager] == nil) {
		AVCamCaptureManager *manager = [[AVCamCaptureManager alloc] init];
		[self setCaptureManager:manager];
		
		[[self captureManager] setDelegate:self];

		if ([[self captureManager] setupSession]) {
            // Create video preview layer and add it to the UI
			AVCaptureVideoPreviewLayer *newCaptureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:[[self captureManager] session]];
			UIView *view = [self videoPreviewView];
			CALayer *viewLayer = [view layer];
			[viewLayer setMasksToBounds:YES];
			
			CGRect bounds = [view bounds];
			[newCaptureVideoPreviewLayer setFrame:bounds];

			[newCaptureVideoPreviewLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
			
			[viewLayer insertSublayer:newCaptureVideoPreviewLayer below:[viewLayer sublayers][0]];
			
			[self setCaptureVideoPreviewLayer:newCaptureVideoPreviewLayer];
			
            // Start the session. This is done asychronously since -startRunning doesn't return until the session is running.
			dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
				[[[self captureManager] session] startRunning];
				NSLog(@"SEssion started");
			});
			
            [self updateButtonStates];
			
            // Create the focus mode UI overlay
			UILabel *newFocusModeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 20, viewLayer.bounds.size.width - 20, 20)];
			[newFocusModeLabel setBackgroundColor:[UIColor clearColor]];
			[newFocusModeLabel setTextColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.50]];
			AVCaptureFocusMode initialFocusMode = [[[self.captureManager videoInput] device] focusMode];
			[newFocusModeLabel setText:[NSString stringWithFormat:@"focus: %@", [self stringForFocusMode:initialFocusMode]]];
			[view addSubview:newFocusModeLabel];
			[self addObserver:self forKeyPath:@"captureManager.videoInput.device.focusMode" options:NSKeyValueObservingOptionNew context:AVCamFocusModeObserverContext];
			[self setFocusModeLabel:newFocusModeLabel];
            
            // Add a single tap gesture to focus on the point tapped, then lock focus
			UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToAutoFocus:)];
			[singleTap setDelegate:self];
			[singleTap setNumberOfTapsRequired:1];
			[view addGestureRecognizer:singleTap];
			
            // Add a double tap gesture to reset the focus mode to continuous auto focus
			UITapGestureRecognizer *doubleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapToContinouslyAutoFocus:)];
			[doubleTap setDelegate:self];
			[doubleTap setNumberOfTapsRequired:2];
			[singleTap requireGestureRecognizerToFail:doubleTap];
			[view addGestureRecognizer:doubleTap];
			
		}		
	}
		
    [super viewDidLoad];
	
	if (prefs.soundsSelected.count == 0) {
		self.playSoundButton.enabled = NO;
		self.playSoundButton.alpha = 0.0;
		self.selectedTuneName.alpha = 0.0;
	} else {
		self.playSoundButton.enabled = 1.0;
		self.playSoundButton.alpha = 1.0;
		// self.selectedTuneName.text = [[ANiGIFDataBase sharedDatabase].sounds objectAtIndex:prefs.tuneSelected];
		self.selectedTuneName.alpha = 1.0;
	}
	
	CGRect tFrame = self.toolsView.frame;
	tFrame.origin.x = -tFrame.size.width;
	self.toolsView.frame = tFrame;
	[self.view addSubview:self.toolsView];

	[self hideRecordingView];
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if (context == AVCamFocusModeObserverContext) {
        // Update the focus UI overlay string when the focus mode changes
		[self.focusModeLabel setText:[NSString stringWithFormat:@"focus: %@", [self stringForFocusMode:(AVCaptureFocusMode)[change[NSKeyValueChangeNewKey] integerValue]]]];
	} else {
        [super observeValueForKeyPath:keyPath ofObject:object change:change context:context];
    }
}

- (void) selectImageDatabase:(NSNotification *)note
{
	[self.imageSelector showViewAtBottom];
}

- (void) selectMusicDatabase:(NSNotification *) note
{
	[self.soundSelector showViewAtBottom];
}

#pragma mark Toolbar Actions
- (IBAction)toggleCamera:(id)sender
{
    // Toggle between cameras when there is more than one
    [[self captureManager] toggleCamera];
	
	[self updateCameraButtonView];
	
    // Do an initial focus
    [[self captureManager] continuousFocusAtPoint:CGPointMake(.5f, .5f)];
}

- (void) updateCameraButtonView
{
	NSString *imName = ([self captureManager].backCameraActive ? @"red-camera-icon" : @"green-camera-icon") ;
	[self.cameraDirectionButton setImage:[UIImage imageNamed:imName] forState:UIControlStateNormal];
}

- (IBAction)toggleRecording:(id)sender
{
    // Start recording if there isn't a recording running. Stop recording if there is.
	if (![[[self captureManager] recorder] isRecording]) {
		// show stop button and start stopper timer
		[self prepareRecordView];
		[[self captureManager] startRecording];
		
	} else {
		// hide stop button
		[self hideRecordingView];
        [[self captureManager] stopRecording];
	}
}

- (void) updateGifPreview:(NSNotification *) note
{
	imageFileRef = [note object];
	if (!imageFileRef) {
		self.gifPreview.image = nil;
	} else {
		UIImage *aniGif = [[ANiGIFDataBase sharedDatabase] animatedGIFForFileRef:imageFileRef];
		self.gifPreview.image = aniGif;
	}
}

- (void) updateTunePreview:(NSNotification *) note
{
	soundFileRef = [note object];
	if (!soundFileRef) {
		self.playSoundButton.enabled = NO;
		self.playSoundButton.alpha = 0.0;
		self.selectedTuneName.alpha = 0.0;
	} else {
		self.playSoundButton.enabled = 1.0;
		self.playSoundButton.alpha = 1.0;
		self.selectedTuneName.text = soundFileRef.caption;
		self.selectedTuneName.alpha = 1.0;
	}
}




- (IBAction)captureStillImage:(id)sender
{
    // Capture a still image
    [[self captureManager] captureStillImage];
    
    // Flash the screen white and fade it out to give UI feedback that a still image was taken
    UIView *flashView = [[UIView alloc] initWithFrame:[[self videoPreviewView] frame]];
    [flashView setBackgroundColor:[UIColor whiteColor]];
    [[[self view] window] addSubview:flashView];
    
    [UIView animateWithDuration:.4f
                     animations:^{
                         [flashView setAlpha:0.f];
                     }
                     completion:^(BOOL finished){
                         [flashView removeFromSuperview];
                     }
     ];
}


- (BOOL)prefersStatusBarHidden
{
	return YES;
}

- (void) prepareRecordView
{
	CGRect mainFrame = self.videoPreviewView.frame;
	// move stop button to proper position
	CGRect controlFrame = self.toolsView.frame;


	// now move subview to the screen
	[UIView beginAnimations : @"Display notif" context:nil];
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:FALSE];
	
	controlFrame.origin.x = - controlFrame.size.width;
	controlFrame.origin.y = (mainFrame.size.height - controlFrame.size.height) * 0.5;
	
	[self.toolsView setFrame:controlFrame];
	self.toolsView.alpha = 0.0;
	
	[UIView commitAnimations];
	
	
	// Init timeoutTimer
	if (recordTimeoutTimer && [recordTimeoutTimer isValid]) {
		[recordTimeoutTimer invalidate];
		recordTimeoutTimer = nil;
	}
	timeout = prefs.recordTimeout;
	recordTimeoutTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self
														selector:@selector(stopRecording:) userInfo:nil repeats:YES];
	self.timeRemained.progress = 1.0;
	self.timeRemained.alpha = 1.0;
}

- (void) hideRecordingView
{
	CGRect mainFrame = self.videoPreviewView.frame;
	// move stop button to proper position
	CGRect controlFrame = self.toolsView.frame;

	// return to center of frame
	// now move subview to the screen
	[UIView beginAnimations : @"Display notif" context:nil];
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:FALSE];

	controlFrame.origin.y =  (mainFrame.size.height - controlFrame.size.height) * 0.5;
	controlFrame.origin.x =  (mainFrame.size.width - controlFrame.size.width) * 0.5;
	self.timeRemained.alpha = 0.0;			// hide progress indicator
	self.toolsView.alpha = 1.0;
	[self.toolsView setFrame:controlFrame];
	
	[UIView commitAnimations];

}


- (void) stopRecording:(NSTimer *)aTimer
{
	timeout -= 1;
	self.toolsRecordPrompt.text = [NSString stringWithFormat:@"%.0f",timeout];
	if (timeout <= 0) {
		[aTimer invalidate];
		recordTimeoutTimer = nil;
		[self hideRecordingView];
		[[NSNotificationCenter defaultCenter] postNotificationName:VVVRecorrdTimeoutFired
																	object:nil];
		[[self captureManager] stopRecording];
		self.timeRemained.alpha = 0.0;
	}
	self.timeRemained.progress = timeout / prefs.recordTimeout;
}

- (IBAction)cameraDirectionButtonPressed:(id)sender {
}


//
// Method called when user preses preplay button with speaker
//
- (IBAction)playSoundButtonPressed:(id)sender
{

	if (prefs.soundsSelected .count == 0) return;	// bullet proof protection
	FileReference *tune = [prefs.soundsSelected anyObject];
	NSString *filename = tune.fileName;
	NSArray *comps = [filename componentsSeparatedByString:@"."];
	// proper filename should have simple DOS-like syntax
	if (!comps || comps.count != 2) return;
	NSString *audioFilePath = [[NSBundle mainBundle] pathForResource:[comps objectAtIndex:0]
															  ofType:[comps objectAtIndex:1]];
	if (!audioFilePath) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"File Not Found!"
														message:[NSString stringWithFormat:@"Audio file %@ not found in aplication!", filename]
													   delegate:nil
											  cancelButtonTitle:@"Ouch! I see..."
											  otherButtonTitles: nil];
		[alert show];
		return;
	}
	NSURL *pathAsURL = [[NSURL alloc] initFileURLWithPath:audioFilePath];
	
	// Init the audio player.
	NSError *error;
	self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:pathAsURL error:&error];
	
	// Check out what's wrong in case that the player doesn't init.
	if (error) {
		UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cannot play file!" message:[error localizedDescription] delegate:nil cancelButtonTitle:@"Ok, I see" otherButtonTitles:nil];
		[alert show];
		return;
	}
	else {
		BOOL prepared = [self.player prepareToPlay];
		if (!prepared) {
			UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Cannot prepare file!"
												message:@"File cennot be prepared to play" delegate:nil cancelButtonTitle:@"Ok, I see" otherButtonTitles:nil];
			[alert show];
			return;
		}
	}
	// delegate is required to auromatically stop playing
	[self.player setDelegate:self];
	// start playing the file
	[self.player play];

}


-(void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag{
	NSLog(@"end!");
}


- (void) showCategorySelector:(NSNotification *) note
{
	[self closeCategorySelector:nil];
	
	BOOL mode = [[note object] boolValue];
	if (!self.categorySelector) {
		self.categorySelector = [[TreeViewController alloc] initForImageSelection:mode];
		self.categorySelector.view.alpha = 0.0;
		CGRect modeScreen = self.modeSelector.view.frame;
		CGRect frame = self.categorySelector.view.frame;
		frame.origin.x = 0;
		frame.origin.y = modeScreen.origin.y - frame.size.height-20;
		[self.categorySelector.view setFrame:frame];
	}
	[self.view addSubview:self.categorySelector.view];
	[UIView animateWithDuration:.4f
					 animations:^{
						 [self.categorySelector.view setAlpha:1.f];
					 }
					 completion:^(BOOL finished){
						 
					 }
	 ];
	
}

- (void) closeCategorySelector:(NSNotification *) note
{
	if (self.categorySelector) {
		[self.categorySelector.view removeFromSuperview];
		self.categorySelector = nil;
	}
}


@end

#pragma mark -

@implementation AVCamViewController (InternalMethods)

// Auto focus at a particular point. The focus mode will change to locked once the auto focus happens.
- (void)tapToAutoFocus:(UIGestureRecognizer *)gestureRecognizer
{
    if ([[[self.captureManager videoInput] device] isFocusPointOfInterestSupported]) {
        CGPoint tapPoint = [gestureRecognizer locationInView:[self videoPreviewView]];
		CGPoint convertedFocusPoint = [self.captureVideoPreviewLayer captureDevicePointOfInterestForPoint:tapPoint];
        [self.captureManager autoFocusAtPoint:convertedFocusPoint];
    }
}

// Change to continuous auto focus. The camera will focus as needed at the point choosen.
- (void)tapToContinouslyAutoFocus:(UIGestureRecognizer *)gestureRecognizer
{
    if ([[[self.captureManager videoInput] device] isFocusPointOfInterestSupported])
        [self.captureManager continuousFocusAtPoint:CGPointMake(.5f, .5f)];
}

// Update button states based on the number of available cameras and mics
- (void)updateButtonStates
{
	// NSUInteger cameraCount = [[self captureManager] cameraCount];
	// NSUInteger micCount = [[self captureManager] micCount];
 }

@end

@implementation AVCamViewController (AVCamCaptureManagerDelegate)

- (void)captureManager:(AVCamCaptureManager *)captureManager didFailWithError:(NSError *)error
{
    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:[error localizedDescription]
                                                            message:[error localizedFailureReason]
                                                           delegate:nil
                                                  cancelButtonTitle:NSLocalizedString(@"OK", @"OK button title")
                                                  otherButtonTitles:nil];
        [alertView show];
    });
}

- (void)captureManagerRecordingBegan:(AVCamCaptureManager *)captureManager
{
//    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
//        [[self recordButton] setTitle:NSLocalizedString(@"Stop", @"eCame recording button stop title")];
//        [[self recordButton] setEnabled:YES];
//    });
}

- (void)captureManagerRecordingFinished:(AVCamCaptureManager *)captureManager
{
//    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
//        [[self recordButton] setTitle:NSLocalizedString(@"Record", @"Toggle recording button record title")];
//        [[self recordButton] setEnabled:YES];
//    });
}

- (void)captureManagerStillImageCaptured:(AVCamCaptureManager *)captureManager
{
//    CFRunLoopPerformBlock(CFRunLoopGetMain(), kCFRunLoopCommonModes, ^(void) {
//        [[self stillButton] setEnabled:YES];
//    });
}

- (void)captureManagerDeviceConfigurationChanged:(AVCamCaptureManager *)captureManager
{
	[self updateButtonStates];
}




@end
