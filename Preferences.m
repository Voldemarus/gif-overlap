//
//  Preferences.m
//  Portal Picture
//
//  Created by Водолазкий В.В. on 30.12.14.
//  Copyright (c) 2014 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "Preferences.h"

#define DEFAULT_TIMEOUT_VALUE	8.0


@interface Preferences () {
	NSUserDefaults *prefs;
	NSMutableSet *imagesSelected;
	NSMutableSet *soundsSelected;
}

@end

// External notificators
NSString * const VVVRecorrdTimeoutFired			=	@"VVVRecorrdTimeoutFired";
NSString * const VVVimageSelected				=	@"VVVinageSelected";
NSString * const VVVfolderSelected				=	@"VVVfolderSelected";
NSString * const VVVselectImageDatabase			=	@"VVVselectImageDatabase";
NSString * const VVVselectMusicDatabase			=	@"VVVselectMusicDatabase";
NSString * const VVVtuneSelected				=	@"VVVtuneSelected";
NSString * const VVVshowCategoryList			=	@"VVVShowCategoryList";
NSString * const VVVcloseCategorySelector		=	@"VVVcloseCategorySelector";
NSString * const VVVstampModeChanged			=	@"VVVstampModeChanged";
NSString * const VVVmainButoonPressed			=	@"VVVmainButoonPressed";
NSString * const VVVmainModeChanged				=	@"VVVmainModeChanged";

// Internal data labels
NSString * const VVVTimeout						=	@"VVVtimeout";
NSString * const VVVStopAtLeft					=	@"VVVStopAtLeft";
NSString * const VVVcategorySelected			=	@"VVVcategorySelected";
NSString * const VVVGIFSelected					=	@"VVVGIFSelected";
NSString * const VVVsoundSelected				=	@"VVVsoundSelected";


@implementation Preferences

@synthesize  imageCategorySelected;
@synthesize  soundCategorySelected;


+ (Preferences *) sharedPreferences
{
	static Preferences *_Preferences;
	if (_Preferences == nil) {
		_Preferences = [[Preferences alloc] init];
	}
	return _Preferences;
}

//
// Init set of data for case when actual preference file is not created yet
//
+ (void)initialize
{
	NSMutableDictionary  *defaultValues = [NSMutableDictionary dictionary];
	// set up default parameters
	[defaultValues setObject:@(DEFAULT_TIMEOUT_VALUE) forKey:VVVTimeout];
	[defaultValues setObject:@(YES) forKey:VVVStopAtLeft];
	[defaultValues setObject:@(-1) forKey:VVVcategorySelected];
	
	[[NSUserDefaults standardUserDefaults] registerDefaults: defaultValues];
	
}

- (id) init
{
	if (self = [super init]) {
		prefs = [NSUserDefaults standardUserDefaults];
		imagesSelected = [[NSMutableSet alloc] init];
		soundsSelected = [[NSMutableSet alloc] init];
	}
	return self;
}


- (void) flush
{
	[[NSUserDefaults standardUserDefaults] synchronize];
}

#pragma mark -


- (NSTimeInterval) recordTimeout
{
	float timeout = [prefs floatForKey:VVVTimeout];
	if (timeout < 1.0) {
		timeout = DEFAULT_TIMEOUT_VALUE;
	}
	return timeout;
}

- (void) setRecordTimeout:(NSTimeInterval)recordTimeout
{
	[prefs setFloat:recordTimeout forKey:VVVTimeout];
}

- (BOOL) stopButtonAtLeft
{
	return [prefs boolForKey:VVVStopAtLeft];
}

- (void) setStopButtonAtLeft:(BOOL)stopButtonAtLeft
{
	[prefs setBool:stopButtonAtLeft forKey:VVVStopAtLeft];
}

- (NSInteger) categorySelected
{
	return [prefs integerForKey:VVVcategorySelected];
}

- (void) setCategorySelected:(NSInteger)categorySelected
{
	[prefs setInteger:categorySelected forKey:VVVcategorySelected];
}

#pragma mark -

- (NSMutableSet *) imagesSelected
{
	return imagesSelected;
}

- (NSMutableSet *) soundsSelected
{
	return soundsSelected;
}

- (void) addSelectedImage:(FileReference *) fref
{
	if (fref) {
		[imagesSelected removeAllObjects];			// limit to 1 object for now
		[imagesSelected addObject:fref];
	}
}

- (void) removeSelectedImage:(FileReference *) fref
{
	if (fref) [imagesSelected removeObject:fref];
}
- (BOOL) imageIsSelected:(FileReference *)fref
{
	return (fref && [imagesSelected containsObject:fref]);
}

- (void) addSelectedSound:(FileReference *)fref
{
	if (fref) {
		[soundsSelected removeAllObjects];		// limit to 1 oblject per collection for now
		[soundsSelected addObject:fref];
	}
}
- (void) removeSelectedSound:(FileReference *)fref
{
	if (fref) [soundsSelected removeObject:fref];
}
- (BOOL) soundIsSelected:(FileReference *)fref;
{
	return (fref && [soundsSelected containsObject:fref]);
}



@end




