//
//  MediaSelectorViewController.m
//  VideoSmile
//
//  Created by Водолазкий В.В. on 12.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "MediaSelectorViewController.h"
#import "DebugPrint.h"
#import "Preferences.h"
#import "AppDelegate.h"


@interface MediaSelectorViewController () {
	FileCategory *selectedCategory;
	Preferences *prefs;
	BOOL selectorOnScreen;
	NSFetchedResultsController *cntrl;
}

@end

@implementation MediaSelectorViewController



- (IBAction)backButtonPressed:(id)sender {
}

- (id) initWithImageList
{
	if (self = [super init]) {
		imageMode = YES;
	}
	return self;
}

- (id) initWithSoundList
{
	if (self = [super init]) {
		imageMode = NO;
	}
	return self;
}

- (BOOL) imageMode
{
	return imageMode;
}

- (void)viewDidLoad {
    [super viewDidLoad];
	prefs = [Preferences sharedPreferences];
	selectorOnScreen = NO;
	
	NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
	[nc addObserver:self selector:@selector(updateCategory:) name:VVVfolderSelected object:nil];
	[nc addObserver:self selector:@selector(updateMainButton:) name:VVVmainModeChanged object:nil];
	
	[self updateCategory:nil];
	
	UINib *cellNib = [UINib nibWithNibName:@"SoundCell" bundle:nil];
	[self.collection registerNib:cellNib forCellWithReuseIdentifier:@"SoundCell"];
	
	cellNib = [UINib nibWithNibName:@"GIFCell" bundle:nil];
	[self.collection registerNib:cellNib forCellWithReuseIdentifier:@"GIFCell"];
	
	UICollectionViewFlowLayout *flowLayout = (UICollectionViewFlowLayout*)self.collection.collectionViewLayout;
	flowLayout.sectionInset = UIEdgeInsetsMake(-15, 10, 0, 10);
	
	[flowLayout setItemSize:CGSizeMake(84, 64)];
	[flowLayout setScrollDirection:UICollectionViewScrollDirectionHorizontal];

	imageMode = YES;
	
	// Disable back button
	self.backButton.alpha = 0.0;
	self.backButton.enabled = NO;
	
	[self updateCategory:nil];
	
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)categorySelectButtonPressed:(id)sender
{
	if (selectorOnScreen) {
		[[NSNotificationCenter defaultCenter] postNotificationName:VVVcloseCategorySelector object:nil];
	} else {
		[[NSNotificationCenter defaultCenter] postNotificationName:VVVshowCategoryList object:@(imageMode)];
	}
	selectorOnScreen = !selectorOnScreen;
}

- (IBAction)mainModeButtonPRessed:(id)sender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVmainButoonPressed object:nil];
}


//
//  Attention! Buttons are taken from iconarchive.com
//
- (void) updateMainButton:(NSNotification *) note
{
	return;  /// TMP!!!
	
	VSMode mode = [(AppDelegate *) [[UIApplication sharedApplication] delegate] workingMode];
	NSString *imageName = nil;
	switch (mode) {
		case VSModeReadyToRecord:		imageName = @"Record-Normal-Red-icon.png"; break;
		case VSModePlayInProgress:		imageName = @"Stop-Normal-Yellow-icon"; break;
		case VSModeReadyToPlay:			imageName = @"Play-Normal-icon"; break;
		case VSModeRecordInProgress:	imageName = @"Stop-Normal-Red-icon"; break;
	}
	self.backButton.alpha = (mode >= VSModeReadyToPlay ? 1.0 : 0.0);
	self.backButton.enabled = (mode >= VSModeReadyToPlay ? 1.0 : 0.0);
}

#pragma mark -


- (void) updateCategory:(NSNotification *) note
{
	if (note) {
		selectedCategory = [note object];
	} else {
		selectedCategory = (imageMode ? prefs.imageCategorySelected : prefs.soundCategorySelected);
	}

	if (selectedCategory) {
		self.categoryLabel.text = selectedCategory.caption;
		cntrl = [[ANiGIFDataBase sharedDatabase] filesForCategory:selectedCategory];
		NSError *error = nil;
		[cntrl performFetch:&error];
		if (error) {
			DLog(@"cannot perform fetch with category - %@ :: %@", selectedCategory, [error localizedDescription]);
		}
	} else {
		self.categoryLabel.text = @"Select category";
		cntrl = nil;
		if (imageMode) {
			prefs.imageCategorySelected = nil;
		} else {
			prefs.soundCategorySelected = nil;
		}
	}
	if (note && selectedCategory) {
		if (imageMode) {
			prefs.imageCategorySelected = selectedCategory;
		} else {
			prefs.soundCategorySelected = selectedCategory;
		}
	}
	[self.collection reloadData];
	selectorOnScreen = NO;
}

- (IBAction)closeModePressed:(id)sender
{
	// hide current view beneath the screen
	CGRect frame = self.view.frame;
	
	[UIView beginAnimations : @"Hide AniGif" context:nil];
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:FALSE];
	
	frame.origin.y = frame.origin.y + frame.size.height;
	
	[self.view setFrame:frame];
	self.view.alpha = 0.0;
	
	[UIView commitAnimations];
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVcloseCategorySelector object:nil];
}

- (IBAction)imageModeSelected:(id)sender
{
	if (imageMode && selectorOnScreen) {
		[[NSNotificationCenter defaultCenter] postNotificationName:VVVcloseCategorySelector object:nil];
	} else {
		imageMode = YES;
		[self updateCategory:nil];
		[[NSNotificationCenter defaultCenter] postNotificationName:VVVshowCategoryList
															object:@(imageMode)];
	}
}

- (IBAction)tuneModeSelected:(id)sender
{
	if (!imageMode && selectorOnScreen) {
		[[NSNotificationCenter defaultCenter] postNotificationName:VVVcloseCategorySelector object:nil];
	} else {
		imageMode = NO;
		[self updateCategory:nil];
		[[NSNotificationCenter defaultCenter] postNotificationName:VVVshowCategoryList
															object:@(imageMode)];
	}
}

- (IBAction)stampModePressed:(id)sender
{
	[[NSNotificationCenter defaultCenter] postNotificationName:VVVstampModeChanged object:nil];
}

- (void) showViewAtBottom
{
	// hide current view beneath the screen
	CGRect frame = self.view.frame;
	CGRect mainFrame = [[UIScreen mainScreen] bounds];
	
	// now move subview to the screen
	[UIView beginAnimations : @"Show AniGif" context:nil];
	[UIView setAnimationDuration:1];
	[UIView setAnimationBeginsFromCurrentState:FALSE];
	
	frame.origin.y = mainFrame.size.height - frame.size.height;
	
	[self.view setFrame:frame];
	self.view.alpha = 1.0;
	
	[UIView commitAnimations];
	
	selectedCategory = (imageMode ? prefs.imageCategorySelected : prefs.soundCategorySelected);
	[self updateCategory:nil];
}

#pragma mark - UICollection view delegate

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return (cntrl ? cntrl.fetchedObjects.count : 0);
}

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
	return 1;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
	
	FileReference *dataObject = [cntrl.fetchedObjects objectAtIndex:indexPath.row];
	
	
	static NSString *soundCellIdentifier = @"SoundCell";
	static NSString *imageCellIdentifier = @"GIFCell";
	
	UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:(imageMode ? imageCellIdentifier : soundCellIdentifier) forIndexPath:indexPath];
	if (imageMode) {
		UIImageView *im = (UIImageView *)[cell viewWithTag:100];
		im.image = [UIImage imageNamed:dataObject.fileName];
	} else {
		UILabel *label = (UILabel*)[cell viewWithTag:100];
		label.text = dataObject.caption;
	}
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	FileReference *fileRef = cntrl.fetchedObjects[indexPath.row];
	BOOL previouslySelected = NO;
	NSString *notificationName = nil;		// to generate error!
	if (imageMode) {
		previouslySelected = [prefs imageIsSelected:fileRef];
		notificationName = VVVimageSelected;
	} else {
		previouslySelected = [prefs soundIsSelected:fileRef];
		notificationName = VVVtuneSelected;
	}
	if (previouslySelected) {
		// deselect current object only
		[self.collection deselectItemAtIndexPath:indexPath animated:YES];
		[[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:nil];
		if (imageMode) {
			[prefs removeSelectedImage:fileRef];
		} else {
			[prefs removeSelectedSound:fileRef];
		}
	} else {
		if (imageMode) {
			[prefs addSelectedImage:fileRef];
		} else {
			[prefs addSelectedSound:fileRef];
		}
		[[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:fileRef];
	}
}




@end
