//
//  FileReference.m
//  VideoSmile
//
//  Created by Водолазкий В.В. on 11.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "FileReference.h"
#import "FileCategory.h"
#import "DebugPrint.h"
#import <ImageIO/ImageIO.h>

@implementation FileReference

@dynamic fileName;
@dynamic caption;
@dynamic category;
@dynamic imageData;
@dynamic frameCount, frameDuration, width, height;


+ (FileReference *) getImageFileReferenceWithName:(NSString *)fname parent:(FileCategory *) parent
											allCat:(FileCategory *)allCat inMoc:(NSManagedObjectContext *) moc
{
	return [FileReference getFileReferenceWithName:fname caption:@""
											parent:parent  allCat:allCat inMoc:moc];
}


+ (FileReference *) getSoundFileReferenceWithName:(NSString *)fname
										caption:(NSString *)caption
										   parent:(FileCategory *) parent
											allCat:(FileCategory *)allCat
											inMoc:(NSManagedObjectContext *) moc
{
	return [FileReference getFileReferenceWithName:fname caption:caption
											parent:parent  allCat:allCat inMoc:moc];
}


+ (FileReference *) getFileReferenceWithName:(NSString *)fname
									 caption:(NSString *)caption
										   parent:(FileCategory *) parent
										allCat:(FileCategory *)allCat
											inMoc:(NSManagedObjectContext *) moc
{
	NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:[[self class] description]];
	NSError *error = nil;
	req.predicate = [NSPredicate predicateWithFormat:@"category = %@ and fileName = %@ and caption = %@",
				 parent, fname, caption];
	NSArray *res = [moc executeFetchRequest:req error:&error];
	if (!res && error) {
		ALog(@"error - %@",[error localizedDescription]);
		abort();
	}
	FileReference *newFCat = nil;
	if (res.count > 0) {
		newFCat =  (FileReference *)[res objectAtIndex:0];
	} else {
		newFCat = [NSEntityDescription insertNewObjectForEntityForName:[[self class] description] inManagedObjectContext:moc];
		if (!newFCat) {
			ALog(@"cannot create new entity - %@", [error localizedDescription]);
			abort();
		}
		newFCat.caption = caption;
		newFCat.fileName = fname;
	}
	if (parent) ([parent addFileToCategory:newFCat]);
	if (allCat) ([allCat addFileToCategory:newFCat]);
	
	if ([[(FileCategory *)parent imageData] boolValue]) {
		fname = [fname stringByDeletingPathExtension];
		NSString *filePath = [[NSBundle mainBundle] pathForResource:fname ofType:@"gif"];
		NSData *data = [NSData dataWithContentsOfFile:filePath];
		
		newFCat.imageData = data;			// stroe data in database to save time on decoding later
		
		CGImageSourceRef srcRef = CGImageSourceCreateWithData((__bridge CFDataRef)data, NULL);
		
		uint32_t const numFrames = (uint32_t) CGImageSourceGetCount(srcRef);
		
		newFCat.frameCount = @(numFrames);
		
		float minDelaySeconds = 10000.0;
		//uint32_t foundHasAlphaFlag = 0;
		
		uint32_t width = 0;
		uint32_t height = 0;
		
		for (int i=0; i < numFrames; i++) {
			CFDictionaryRef imageFrameProperties = CGImageSourceCopyPropertiesAtIndex(srcRef, i, NULL);
			assert(imageFrameProperties);
			
			CFDictionaryRef gifProperties = CFDictionaryGetValue(imageFrameProperties, kCGImagePropertyGIFDictionary);
			assert(gifProperties);
			
			// kCGImagePropertyGIFDelayTime is rounded up to 0.1 if smaller than 0.1.
			// kCGImagePropertyGIFUnclampedDelayTime is the original value in the GIF file
			
			CFNumberRef delayTime = CFDictionaryGetValue(gifProperties, kCGImagePropertyGIFUnclampedDelayTime);
			assert(delayTime);
			
			NSNumber *delayTimeNum;
			delayTimeNum = (__bridge NSNumber*)delayTime;
			
			// ImageIO must return the delay time in seconds
			
			float delayTimeFloat = (float) [delayTimeNum doubleValue];
			
			// Define a lower limit of about 30 FPS. The clamped value defined by kCGImagePropertyGIFDelayTime
			// is too restrictive since a value of 0.04 corresponds to about 23 fps.
			
			if (delayTimeFloat <= (1.0f/30.0f)) {
				delayTimeFloat = (1.0f/30.0f);
			}
			
			if (delayTimeFloat < minDelaySeconds) {
				minDelaySeconds = delayTimeFloat;
			}
			
			if (width == 0) {
				CFNumberRef pixelWidth = CFDictionaryGetValue(imageFrameProperties, @"PixelWidth");
				CFNumberRef pixelHeight = CFDictionaryGetValue(imageFrameProperties, @"PixelHeight");
				
				NSNumber *pixelWidthNum;
				NSNumber *pixelHeightNum;

				pixelWidthNum = (__bridge NSNumber*)pixelWidth;
				pixelHeightNum = (__bridge NSNumber*)pixelHeight;
				
				width = [pixelWidthNum unsignedIntValue];
				height = [pixelHeightNum unsignedIntValue];
			}
			newFCat.width = @(width);
			newFCat.height = @(height);
			
			CFRelease(imageFrameProperties);
		}
		
		newFCat.frameDuration = @(minDelaySeconds);
		
		//
		// Parameters have been ready, now we need to decompose each frame and
		// store it in a way, suitable for fast processing
		//
		
		CFRelease(srcRef);
	}
	
	
	return newFCat;
}


- (NSString *) description
{
	return [NSString stringWithFormat:@"%@: %@ -> %@  (%@*%@)  Frames:%@, FrameDelay:%@",
			[self class], self.caption, self.fileName, self.width, self.height, self.frameCount, self.frameDuration];
}

@end
