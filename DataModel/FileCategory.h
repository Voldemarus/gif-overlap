//
//  Category.h
//  VideoSmile
//
//  Created by Водолазкий В.В. on 11.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

#import "FileReference.h"

@interface FileCategory : NSManagedObject

@property (nonatomic, retain) NSString * caption;
@property (nonatomic, retain) NSNumber *imageData;
@property (nonatomic, retain) NSSet *files;
@property (nonatomic, retain) NSSet *subcategories;
@property (nonatomic, retain) FileCategory *parent;

@property (nonatomic, retain, readonly) NSArray *subcatOrdered;

+ (FileCategory *) getFileCategoryWithCaption:(NSString *)cat parent:(FileCategory *)parent
								   andImageData:(BOOL)imageData inMoc:(NSManagedObjectContext *)moc;
- (void) addFileToCategory:(FileReference *)aFref;

@end

@interface FileCategory (CoreDataGeneratedAccessors)

- (void)addFilesObject:(FileReference *)value;
- (void)removeFilesObject:(FileReference *)value;
- (void)addFiles:(NSSet *)values;
- (void)removeFiles:(NSSet *)values;

- (void)addSubcategoriesObject:(FileCategory *)value;
- (void)removeSubcategoriesObject:(FileCategory *)value;
- (void)addSubcategories:(NSSet *)values;
- (void)removeSubcategories:(NSSet *)values;

@end
