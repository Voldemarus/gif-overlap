//
//  FileReference.h
//  VideoSmile
//
//  Created by Водолазкий В.В. on 11.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class FileCategory;

@interface FileReference : NSManagedObject

@property (nonatomic, retain) NSString * fileName;
@property (nonatomic, retain) NSString * caption;
@property (nonatomic, retain) NSNumber *frameCount;
@property (nonatomic, retain) NSNumber *frameDuration;
@property (nonatomic, retain) NSNumber *width;
@property (nonatomic, retain) NSNumber *height;
@property (nonatomic, retain) NSData *imageData;

@property (nonatomic, retain) NSSet *category;

+ (FileReference *) getImageFileReferenceWithName:(NSString *)fname parent:(FileCategory *) parent
										   allCat:(FileCategory *)allCat
											inMoc:(NSManagedObjectContext *) moc;
+ (FileReference *) getSoundFileReferenceWithName:(NSString *)fname caption:(NSString *)caption
										   parent:(FileCategory *) parent
										   allCat:(FileCategory *)allCat
											inMoc:(NSManagedObjectContext *) moc;


@end
