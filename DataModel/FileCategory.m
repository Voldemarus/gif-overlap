//
//  Category.m
//  VideoSmile
//
//  Created by Водолазкий В.В. on 11.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "FileCategory.h"
#import "FileCategory.h"
#import "FileReference.h"
#import "DebugPrint.h"

@implementation FileCategory

@dynamic caption;
@dynamic files;
@dynamic imageData;
@dynamic subcategories;
@dynamic parent;

- (NSString *)description
{
	return [NSString stringWithFormat:@"%@ : %@ (%ld/%ld) %@",
			[self class], self.caption, (unsigned long)[self.subcategories count], (unsigned long)[self.files count], ([self.imageData boolValue] ? @"Image" : @"Sound")];
}

+ (FileCategory *)getFileCategoryWithCaption:(NSString *)cat parent:(FileCategory *)parent
								   andImageData:(BOOL)imageData inMoc:(NSManagedObjectContext *)moc
{
	NSFetchRequest *req = [[NSFetchRequest alloc] initWithEntityName:[[self class] description]];
	NSError *error = nil;
	req.predicate = [NSPredicate predicateWithFormat:@"caption = %@ AND parent = %@ and imageData = %@", cat, parent, @(imageData)];
	NSArray *res = [moc executeFetchRequest:req error:&error];
	if (!res && error) {
		ALog(@"error - %@",[error localizedDescription]);
		abort();
	}
	if (res.count > 0) {
		return (FileCategory *)[res objectAtIndex:0];
	}
	FileCategory *newFCat = [NSEntityDescription insertNewObjectForEntityForName:[[self class] description] inManagedObjectContext:moc];
	if (!newFCat) {
		ALog(@"cannot create new entity - %@", [error localizedDescription]);
		abort();
	}
	newFCat.caption = cat;
	newFCat.parent = parent;
	newFCat.imageData = @(imageData);
	
	DLog(@"%@",newFCat);
	
	return newFCat;
}

- (void) addFileToCategory:(FileReference *)aFref
{
	[self addFilesObject:aFref];
}

- (NSArray *) subcatOrdered
{
	if (self.subcategories.count == 0) {
		return @[];
	}
	NSArray *tmp = [self.subcategories allObjects];
	return tmp;
}

@end
