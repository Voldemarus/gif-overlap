//
//  ANiGIFDataBase.m
//  VideoSmile
//
//  Created by Водолазкий В.В. on 06.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import "ANiGIFDataBase.h"
#import "UIImage+animatedGIF.h"
#import "DebugPrint.h"
#import <ImageIO/ImageIO.h>

@implementation ANiGIFDataBase

@synthesize managedObjectContext, managedObjectModel,
		persistentStoreCoordinator = _persistentStoreCoordinator;

+ (ANiGIFDataBase *) sharedDatabase
{
	static ANiGIFDataBase *_database;
	if (_database == nil) {
		_database = [[ANiGIFDataBase alloc] init];
	}
	return _database;
}

- (id) init
{
	if (self = [super init]) {
		NSFetchedResultsController *r = [self imageRootController];
		NSError *error = nil;
		[r performFetch:&error];
		if (error) {
			DLog(@"error during database intializing - %@",[error localizedDescription]);
			abort();
			return nil;
		}
		NSInteger loaded = [r fetchedObjects].count;
		if (loaded == 0) {
			// Need to initialize database
			NSDictionary *database = [self allData];
			for (NSString *key in database) {
				BOOL imageData = [key isEqualToString:@"image"];
				NSDictionary *d = [database objectForKey:key];
				// first of all create All category, which will hold all images
				FileCategory *allCat = [FileCategory getFileCategoryWithCaption:@"All"
						 parent:nil	andImageData:imageData	inMoc:self.managedObjectContext];
				// scan top level categories
				for (NSString *cat in d) {
					id entry = [d objectForKey:cat];
					FileCategory *topCat = [FileCategory getFileCategoryWithCaption:cat parent:nil
							andImageData:imageData	inMoc:self.managedObjectContext];
					// this is a leaf-ed branch of tree
					if ([entry isKindOfClass:[NSArray class]]) {
						[self processLeafBranch:entry withImages:imageData  allCat:allCat andParent:topCat];
					} else {
						// this is a subcategory - should create daughter category and process it
						for (NSString *sCat in (NSDictionary *)entry) {
							FileCategory *subCat = [FileCategory getFileCategoryWithCaption:sCat parent:topCat
														andImageData:imageData	inMoc:self.managedObjectContext];
							NSArray *list = [entry objectForKey:sCat];
							[self processLeafBranch:list withImages:imageData allCat:allCat andParent:subCat];
						}
					}
				}
			}
			[self saveContext];	// fix data as warehouse
			r = [self imageRootController];
#pragma unused (r)
		}
	}
	return self;
}

- (void) processLeafBranch:(id)entry withImages:(BOOL) imageData allCat:(FileCategory *)allCat
				 andParent:(FileCategory *)topCat
{
	for (id object in (NSArray *)entry) {
		if (imageData) {
			// simplest case - only list of records
			FileReference *nr = [FileReference getImageFileReferenceWithName:(NSString *)object
																	  parent:topCat
																	  allCat:allCat
																	   inMoc:self.managedObjectContext ];
			DLog(@"%@", nr);
		} else {
			NSString *title= object[0];
			NSString *filename = object[1];
			[FileReference getSoundFileReferenceWithName:filename
												 caption:title
												  parent:topCat
												  allCat:allCat
												   inMoc:self.managedObjectContext];
		}

	}
	
}


#pragma mark - Basic Core Data methods

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
	if (managedObjectModel != nil) {
		return managedObjectModel;
	}
	NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"VideoSmile" withExtension:@"momd"];
	managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
	if (!managedObjectModel) {
		DLog(@"Cannot initalize data model");
		abort();
	}
	return managedObjectModel;
}


#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *) applicationDocumentsDirectory
{
	return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory
												   inDomains:NSUserDomainMask] lastObject];
}


// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.

- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
	if (_persistentStoreCoordinator != nil) {
		return _persistentStoreCoordinator;
	}
	
	NSURL *storeURL = [[self applicationDocumentsDirectory]
					   URLByAppendingPathComponent:@"VideoSmaile.sqlite"];
	
	NSError *error = nil;
	_persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
	NSDictionary *options = @{NSMigratePersistentStoresAutomaticallyOption: @YES, NSInferMappingModelAutomaticallyOption: @YES,NSSQLitePragmasOption:@{@"journal_mode":@"DELETE"}};
	
	while (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:options error:&error]) {
				DLog(@"Unresolved error %@, %@", error, [error userInfo]);
				abort();
	}
	return _persistentStoreCoordinator;
}

- (NSManagedObjectContext *)managedObjectContext
{
	if (managedObjectContext != nil) {
		return managedObjectContext;
	}
	NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
	if (coordinator != nil) {
		managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
		// Устанавливаем интервал жизни в кэше для неиспользуемых данных
		[managedObjectContext setStalenessInterval:5.0];
		[managedObjectContext setPersistentStoreCoordinator:coordinator];
	}
	return managedObjectContext;
}


#pragma mark - Saving
//
// self.moc используется для поддержки UI элементов
//
- (void)saveContext
{
	[self saveContext:self.managedObjectContext sync:YES completion:nil];
}

- (void)saveContext:(NSManagedObjectContext *)context sync:(BOOL)sync completion:(void (^)())completion {
	__block BOOL hasChanges = NO;
	[context performBlockAndWait:^{
		hasChanges = [context hasChanges];
	}];
	
	if (!hasChanges) {
		if (completion) {
			dispatch_async(dispatch_get_main_queue(), ^{
				completion();
			});
		}
		return;
	}
	id saveBlock = ^{
		BOOL saveResult = NO;
		NSError *error = nil;
		
		@try {
			saveResult = [context save:&error];
		}
		@catch(NSException *exception) {
			DLog(@"SAVING CONTEXT HAS EXCEPTION");
		}
		@finally {
			if (error) {
				DLog(@"error during saving - %@", [error localizedDescription]);
			}
			if ([context parentContext]) {
				[self saveContext:context.parentContext sync:sync completion:completion];
			} else {
				if (completion) {
					dispatch_async(dispatch_get_main_queue(), ^{
						completion();
					});
				}
			}
		}
	};
	
	if (sync) {
		[context performBlockAndWait:saveBlock];
	} else {
		[context performBlock:saveBlock];
	}
}

#pragma mark - fetch controllers -

- (NSFetchedResultsController *) imageRootController
{
	return [self imagesForCategory:nil];
}

- (NSFetchedResultsController *) soundRootController
{
	return [self soundsForCategory:nil];
}

- (NSFetchedResultsController *) imagesForCategory:(FileCategory *)cat
{
	return [self controllerForCategory:cat withImages:YES];
}
- (NSFetchedResultsController *) soundsForCategory:(FileCategory *)cat
{
	return [self controllerForCategory:cat withImages:NO];
}

- (NSFetchedResultsController *) controllerForCategory:(FileCategory *)cat withImages:(BOOL) isImage
{
	NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"FileCategory"];
	request.predicate = [NSPredicate predicateWithFormat:@"parent = %@ AND imageData = %@",cat, @(isImage)];
	request.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"caption" ascending:YES]];
	
	NSFetchedResultsController *fetch = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
	[fetch performFetch:nil];
	return fetch;
}


- (NSFetchedResultsController *) filesForCategory:(FileCategory *)cat
{
	NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"FileReference"];
	request.predicate = [NSPredicate predicateWithFormat:@" %@ IN category",cat];
	request.sortDescriptors = @[ [NSSortDescriptor sortDescriptorWithKey:@"caption" ascending:YES]];
	
	NSFetchedResultsController *fetch = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
	[fetch performFetch:nil];
	return fetch;
}


#pragma mark -


- (NSArray *) categories
{
	return [[self allData] allKeys];
}


- (NSArray *) sounds
{
	return [[self allSounds] allKeys];
}

- (UIImage *) coverForCategory:(NSString *) categoryName
{
	NSArray *catData = [[self allData] objectForKey:categoryName];
	if (!catData) return nil;			// no such category found
	NSString *imageName = [catData objectAtIndex:0];		// first record is used as cover
	UIImage *image = [UIImage imageNamed:imageName];
	return image;
}

- (NSInteger) imageCountforCategory:(NSString *)categoryName
{
	NSArray *catData = [[self allData] objectForKey:categoryName];
	if (!catData) return 0;			// no such category found
	return catData.count;

}

- (UIImage *) animatedGIFForFileRef:(FileReference *)fileRef
{
	return [UIImage animatedImageWithAnimatedGIFData:fileRef.imageData];
}


- (void) updateFramesSequence
{
	
		// Check "HasAlpha" property for each frame, if an earlier frame does not contain
		// a transparent pixel but a later frame does, then all frames need to be treated.
		// This should work, but it actually does not work better than detection since it
		// appears that images that really are 24BPP get detected as 32 BPP. Go with the
		// scanning approach on actual rendered pixels since that works in all cases.
		
		//    if (foundHasAlphaFlag == 0) {
		//      CFNumberRef hasAlpha = CFDictionaryGetValue(imageFrameProperties, @"HasAlpha");
		//
		//      NSNumber *hasAlphaNum = (NSNumber*)hasAlpha;
		//
		//      uint32_t hasAlphaValue = [hasAlphaNum intValue];
		//      if (hasAlphaValue == 1) {
		//        foundHasAlphaFlag = 1;
		//      }
		//    }
		
	
	// Create .mvid file writer utility object
	
	//	aVMvidFileWriter = [AVMvidFileWriter aVMvidFileWriter];
	//
	//	aVMvidFileWriter.mvidPath = outMaxvidPath;
	//	aVMvidFileWriter.frameDuration = frameDuration;
	//	aVMvidFileWriter.totalNumFrames = numFrames;
	//	aVMvidFileWriter.movieSize = CGSizeMake(width, height);
	//	aVMvidFileWriter.genAdler = genAdler;
	
	//	BOOL worked = [aVMvidFileWriter open];
	//
	//	if (worked == FALSE) {
	//		RETCODE(WRITE_ERROR);
	//	}
	
	/*
	 // Iterate over each frame, extract the image and write the data
	 // to the mvid file. Make sure to cleanup the autorelease pool so
	 // that not all the image frames are actually loaded into memory.
	 
	 cgFrameBuffer = [CGFrameBuffer cGFrameBufferWithBppDimensions:32 width:width height:height];
	 uint32_t detectedBpp = 24;
	 //if (foundHasAlphaFlag) {
	 //  detectedBpp = 32;
	 //}
	 
	 for (int i=0; i < numFrames; i++) @autoreleasepool {
		CGImageRef imgRef = CGImageSourceCreateImageAtIndex(srcRef, i, NULL);
		
		// if scan finds no transparent pixels, then detectedBpp = 24
		
		// Render the image contents into a framebuffer. We don't know what
		// the exact binary layout of the GIF image data might be, though it
		// is likely to be a flat array of 32 BPP pixels.
		
		uint32_t imageWidth  = (uint32_t) CGImageGetWidth(imgRef);
		uint32_t imageHeight = (uint32_t) CGImageGetHeight(imgRef);
		
		assert(imageWidth == width);
		assert(imageHeight == height);
		
		[cgFrameBuffer clear];
		[cgFrameBuffer renderCGImage:imgRef];
		
		// Scan the framebuffer to determine if any transparent pixels appear
		// in the frame. This is not fast, but it works. A better approach
		// would be to inspect the metadata to see if it indicates when
		// transparent pixels appear in the color table.
		
		uint32_t *pixels = (uint32_t*) cgFrameBuffer.pixels;
		
		if (detectedBpp == 24) {
	 for (int rowi = 0; rowi < height; rowi++) {
	 for (int coli = 0; coli < width; coli++) {
	 uint32_t pixel = pixels[(rowi * width) + coli];
	 uint8_t alpha = (pixel >> 24) & 0xFF;
	 if (alpha != 0xFF) {
	 detectedBpp = 32;
	 break;
	 }
	 }
	 }
		}
		
		CGImageRelease(imgRef);
		
		// Write the keyframe to the output file
		
		int numBytesInBuffer = (int) cgFrameBuffer.numBytes;
		
		worked = [aVMvidFileWriter writeKeyframe:cgFrameBuffer.pixels bufferSize:numBytesInBuffer];
		
		assert(worked);
	 }
	 
	 // Write .mvid header again, now that info is up to date
	 
	 aVMvidFileWriter.bpp = detectedBpp;
	 
	 worked = [aVMvidFileWriter rewriteHeader];
	 
	 if (worked == FALSE) {
		RETCODE(WRITE_ERROR);
	 }
	 
	 retcode:
	 CFRelease(srcRef);
	 
	 [aVMvidFileWriter close];
	 
	 }
	 */
	
}


#pragma mark -



- (NSString *)soundFileNameFortuneName:(NSString *)aName
{
	return [[self allSounds] objectForKey:aName];
}

#pragma mark -

- (NSDictionary *) allData {
	static NSDictionary *_allData = nil;
	if (!_allData) {
		NSDictionary *tmp =  @{
				@"image" : [self imageData],
				@"sound" : [self allSounds],
			 };
		_allData = [[NSDictionary alloc] initWithDictionary:tmp copyItems:YES];
	}
	return _allData;
}


- (NSDictionary *) imageData
{
	return @{
				@"Humor"  :  @[
						@"spongebob.gif",
						],
				@"Events": @[
								@"happy-bday.gif",
								@"BallonsSparkle.gif",
								@"stars.gif",
						],
				@"Love" : @[
						@"heart-4.gif",
						@"stars.gif",
						],
				@"Festival" : @[
						@"clapping.gif",
						],
				@"Cartoon"  :  @[
						@"spongebob.gif",
						],
				@"TV":  @[
						@"stars.gif",
						],
				@"Movie":  @[
						@"stars.gif",
						],
					
			 };
}

- (NSDictionary *) allSounds
{
	return @{
			 @"Events":    @{
					 @"Birthday" : @[
								@[ @"Happy Burthday",	@"Happybday.mp3"],
							 ],
					 @"Aniversary" : @[
								@[ @"English", @"movie-2.m4a"],
							 ]
			 },
			 @"Movie":	@{
					 @"Action" :	@[
							 @[ @"James Bond Tune",	@"jamesbond.m4a"],
							 @[ @"Chineese", @"movie1.m4a"],
							 ],
					 @"Classic" :  @[
							 @[ @"English",	@"movie-2.m4a"],
							 @[ @"Chineese", @"movie1.m4a"],
							]
			 },
			 @"Animals":	@[
						@[ @"Baboon", @"Baboon.mp3"],
						@[ @"Cat",	@"DomesticCat.mp3"],
					 ],
			 @"Simple Set" :	@[
					 	@[ @"English",	@"movie-2.m4a"],
						@[ @"Chineese", @"movie1.m4a"],
						@[ @"James Bond Tune",	@"jamesbond.m4a"],
					 ]
			 };
}


@end
