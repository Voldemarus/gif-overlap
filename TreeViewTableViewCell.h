//
//  TreeViewCellTableViewCell.h
//  VideoSmile
//
//  Created by Водолазкий В.В. on 12.03.15.
//  Copyright (c) 2015 Geomatix Laboratoriess S.R.O. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TreeViewTableViewCell : UITableViewCell

@property (retain, nonatomic) IBOutlet UIImageView *disclosureIcon;
@property (retain, nonatomic) IBOutlet UILabel *categoryLabel;

- (void) setupViewWithCategoryName:(NSString *)caption
						isSelected:(BOOL) selected
						  andImage:(BOOL)showimage;

@end
